Pod::Spec.new do |s|
  s.name             = 'N7MobiWine'
  s.version          = '0.1.14'
  s.summary          = 'WineClassifier for detection and mark recognized wine labels.'

  s.description      = <<-DESC
  WineClassifier for detection and mark recognized wine labels. Solution created for Talkin'' Things app.
                       DESC

  s.homepage         = 'https://bitbucket.org/n7mobile/n7mobiwine'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Hubert Andrzejewski' => 'handrzejewski@n7mobile.com' }
  s.source           = { :git => 'https://gitlab.talkinthings.com/libs/n7mobiwine.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'
  s.static_framework = true

  s.source_files = 'N7MobiWine/Classes/**/*'
  
  s.pod_target_xcconfig = {
    'VALID_ARCHS' => 'x86_64 armv7 arm64',
  }
  
  s.dependency 'PromisesObjC', '~> 1.2.8'
  s.dependency 'Firebase/Core', '~> 6.10.0'
  s.dependency 'Firebase/MLVision', '~> 6.10.0'
  s.dependency 'Firebase/MLVisionTextModel', '~> 6.10.0'
  s.dependency 'Firebase/MLVisionLabelModel', '~> 6.10.0'
  s.dependency 'Firebase/MLVisionAutoML', '~> 6.10.0'
  
end
